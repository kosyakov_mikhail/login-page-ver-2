//
//  LoginPage.swift
//  LogIn Page ver. 2
//
//  Created by Macos on 24.05.2022.
//

import UIKit
import SnapKit
import FirebaseAuth

class LogInController: UIViewController {

    private var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "LogIn Page ver. 2"
        view.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        view.textColor = .init(white: 1, alpha: 1)
        return view
    }()
    
    
    private var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 0.97, green: 0.78, blue: 0.11, alpha: 1)
        button.setTitle("LOG IN", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    private var usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "EMAIL"
        label.textColor = UIColor(red: 0.97, green: 0.77, blue: 0.11, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    private var userNameTextField: UITextField = {
        let username = UITextField()
        username.attributedPlaceholder = NSAttributedString(string: "example@gmail.com", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        username.textColor = .white
        username.borderStyle = .none
        username.autocorrectionType = .no
        return username
    }()
    
    private var passwordLabel: UILabel = {
        let label = UILabel()
        label.text = "PASSWORD"
        label.textColor = UIColor(red: 0.97, green: 0.77, blue: 0.11, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    private var passwordTextField: UITextField = {
        let password = UITextField()
        password.attributedPlaceholder = NSAttributedString(string: "********", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        password.isSecureTextEntry = true
        password.textColor = .white
        password.borderStyle = .none
        password.autocorrectionType = .no
        return password
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = true
        
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.userNameTextField.addLine(position: .bottom, color: .white, height: 2.00)
        self.passwordTextField.addLine(position: .bottom, color: .white, height: 2.00)
        setupLayout()
    }

    // MARK: - Selectors
    
    @objc func handleLogin() {
        guard let email = userNameTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        logUserIn(withEmail: email, password: password)
    }
    
    @objc func goToHome() {
        navigationController?.pushViewController(HomeController(), animated: true)
    }
    
    
    // MARK: - API
        
    func logUserIn(withEmail email: String, password: String ) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if let error = error {
                print("Failed to sign user in with error:", error.localizedDescription)
                return
            }
            
            print("Succesfully logged user in..")
            self.goToHome()
        }
        
        
    }
    
    private func setupLayout() {
        view.backgroundColor = UIColor(red: 1, green: 0.35, blue: 0.28, alpha: 1)
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.centerX.equalToSuperview()
        }
        

        view.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(40)
            make.top.equalTo(titleLabel.snp.bottom).offset(250)

        }

        view.addSubview(userNameTextField)
        userNameTextField.snp.makeConstraints { make in
            make.top.equalTo(usernameLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }

        view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(40)
            make.top.equalTo(userNameTextField.snp.bottom).offset(15)
        }

        view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { make in
            make.top.equalTo(passwordLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }
        
        view.addSubview(loginButton)
        loginButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(passwordTextField.snp.bottom).offset(40)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
    }
    
}




extension UIView {
    enum Line_Position {
        case top
        case bottom
    }

    func addLine(position : Line_Position, color: UIColor, height: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)

        let metrics = ["width" : NSNumber(value: height)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))

        switch position {
        case .top:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .bottom:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}

// MARK: - UITextFieldDelegate

extension LogInController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        
        case userNameTextField:
            userNameTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        break
        case passwordTextField:
            passwordTextField.resignFirstResponder()
        break
        default:
        break
            
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}
