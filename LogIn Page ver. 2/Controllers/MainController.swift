//
//  ViewController.swift
//  LogIn Page ver. 2
//
//  Created by Macos on 24.05.2022.
//

import UIKit
import SnapKit

class MainController: UIViewController {

    private var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "LogIn Page ver. 2"
        view.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        view.textColor = .init(white: 1, alpha: 1)
        return view
    }()
    
    private var signupButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.white
        button.setTitle("SIGN UP", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(goToSignUp), for: .touchUpInside)
        return button
    }()
    
    private var signInButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 0.97, green: 0.78, blue: 0.11, alpha: 1)
        button.setTitle("SIGN IN", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(goToLogIn), for: .touchUpInside)
        return button
    }()
    
    
    @objc fileprivate func goToLogIn() {
        let logInPage = LogInController()
        navigationController?.pushViewController(logInPage, animated: true)
    }
    
    @objc fileprivate func goToSignUp() {
        let signUpPage = SignUpController()
        navigationController?.pushViewController(signUpPage, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = true
        
        
        setupLayout()
    }

    private func setupLayout() {
        view.backgroundColor = UIColor(red: 1, green: 0.35, blue: 0.28, alpha: 1)
        navigationController?.navigationBar.isHidden = true
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.centerX.equalToSuperview()
        }

        view.addSubview(signupButton)
        signupButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(200)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }

        view.addSubview(signInButton)
        signInButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(signupButton.snp.bottom).offset(20)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
        
    }
    

    
}




