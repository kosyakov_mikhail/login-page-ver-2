//
//  SignUpController.swift
//  LogIn Page ver. 2
//
//  Created by Macos on 25.05.2022.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class SignUpController: UIViewController {

    private var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "LogIn Page ver. 2"
        view.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        view.textColor = .init(white: 1, alpha: 1)
        return view
    }()
    
    private var emailTextField: UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.textColor = .white
        textField.borderStyle = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    private var fullnameTextField: UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: "Full Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.textColor = .white
        textField.borderStyle = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    private var usernameTextField: UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.textColor = .white
        textField.borderStyle = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    private var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.isSecureTextEntry = true
        textField.textColor = .white
        textField.borderStyle = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    private var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 0.97, green: 0.78, blue: 0.11, alpha: 1)
        button.setTitle("SIGN UP", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(showLogin), for: .touchUpInside)
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    
    private var containerImage: UIView = {
        let image = UIImageView()
        image.image = UIImage(named: "appstore")
        return image
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = true
        
        self.emailTextField.delegate = self
        self.fullnameTextField.delegate = self
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self

        self.emailTextField.addLine(position: .bottom, color: .white, height: 2.00)
        self.fullnameTextField.addLine(position: .bottom, color: .white, height: 2.00)
        self.usernameTextField.addLine(position: .bottom, color: .white, height: 2.00)
        self.passwordTextField.addLine(position: .bottom, color: .white, height: 2.00)
        setupLayout()
    }
    
    // MARK: - Selectors
    
    @objc func handleSignUp() {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let username = usernameTextField.text else { return }
        guard let fullName = fullnameTextField.text else { return }
        
        createUser(withEmail: email, password: password, fullname: fullName, username: username)
    }
    
    @objc func showLogin() {
        navigationController?.pushViewController(LogInController(), animated: true)
    }
    
    
    
    // MARK: - API
    
    func createUser(withEmail email: String, password: String, fullname: String, username: String) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            
            if let error = error {
                print("Failed to sign user up with errors:", error.localizedDescription)
            }
            
            guard let uid = result?.user.uid else { return }
            
            let values = ["email": email, "username": username]
            
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
                if let error = error {
                    print("Failed to update database values with error:", error.localizedDescription)
                    return
                }
                
                print("Successfully signed user up..")
            })
        }
    }
    

    private func setupLayout() {
        view.backgroundColor = UIColor(red: 1, green: 0.35, blue: 0.28, alpha: 1)
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(350)
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }
        
        view.addSubview(fullnameTextField)
        fullnameTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(emailTextField.snp.bottom).offset(15)
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }
        
        view.addSubview(usernameTextField)
        usernameTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(fullnameTextField.snp.bottom).offset(15)
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }
        
        view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(usernameTextField.snp.bottom).offset(15)
            make.height.equalTo(40)
            make.left.right.equalToSuperview().inset(40)
        }
        
        view.addSubview(signUpButton)
        signUpButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(passwordTextField.snp.bottom).offset(50)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
        view.addSubview(containerImage)
        containerImage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.width.equalTo(100)
            make.height.equalTo(100)
        }
            
        containerImage.layer.masksToBounds = true
        containerImage.layer.cornerRadius = 50
    }
}

extension SignUpController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        
        case emailTextField:
            emailTextField.resignFirstResponder()
            fullnameTextField.becomeFirstResponder()
        break
        case fullnameTextField:
            fullnameTextField.resignFirstResponder()
            usernameTextField.becomeFirstResponder()
        break
        case usernameTextField:
            usernameTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            passwordTextField.resignFirstResponder()
        default:
        break
            
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}
