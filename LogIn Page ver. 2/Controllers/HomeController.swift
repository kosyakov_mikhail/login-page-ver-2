//
//  HomeController.swift
//  LogIn Page ver. 2
//
//  Created by Macos on 26.05.2022.
//

import UIKit

class HomeController: UIViewController {
    
    private var succesfullyLabel: UILabel = {
        let label = UILabel()
        label.text = "You logged in"
        label.font = UIFont(name: "helvetica", size: 30)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var goMainButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(red: 0.97, green: 0.78, blue: 0.11, alpha: 1)
        button.setTitle("GO TO MAIN", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(goToMain), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
//        view.translatesAutoresizingMaskIntoConstraints = false

        
        setupLayout()
    }
    
    
    // MARK: - Selectors
    @objc func goToMain() {
        navigationController?.pushViewController(MainController(), animated: true)
    }

    func setupLayout() {
        view.backgroundColor = .white
        
        view.addSubview(succesfullyLabel)
        succesfullyLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
        view.addSubview(goMainButton)
        goMainButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(succesfullyLabel.snp.bottom).offset(100)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
    }

}
